import path from "path";
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);

export const OUT_DIR_NAME = "public";
export const repoRoot = path.join(__filename, "../..");
