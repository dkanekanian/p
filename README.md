# Portfolio

My personal portfolio website

[![pipeline status](https://gitlab.com/dkanekanian/dkanekanian.gitlab.io/badges/master/pipeline.svg)](https://gitlab.com/dkanekanian/dkanekanian.gitlab.io/-/commits/master)
[![coverage report](https://gitlab.com/dkanekanian/dkanekanian.gitlab.io/badges/master/coverage.svg)](https://gitlab.com/dkanekanian/dkanekanian.gitlab.io/-/commits/master)
[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/dkanekanian/dkanekanian.gitlab.io)