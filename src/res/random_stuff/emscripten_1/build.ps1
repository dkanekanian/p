# Run emscripten in c++ mode with template html file, and allowing c++
# functions to be called from javascript.
em++ .\helloworld.cpp -s WASM=1 -o index.html --shell-file  ..\my_template.html -s NO_EXIT_RUNTIME=1 -s "EXTRA_EXPORTED_RUNTIME_METHODS=['ccall']"
